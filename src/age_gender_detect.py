import numpy as np
import cv2
from typing import List, Tuple
from utils import get_face_boxes, get_age_gender
import streamlit as st
from PIL import Image


st.set_page_config(
    page_title="Open CV demo", layout="centered"
)


st.title("Predict age and gender with OpenCV")
st.markdown("---")


@st.cache_resource
def load_face_model() -> cv2.dnn.Net:
    return cv2.dnn.readNet(
        model="data/model/opencv_face_detector.pbtxt",
        config="data/model/opencv_face_detector_uint8.pb"
    )


@st.cache_resource
def load_age_model() -> cv2.dnn.Net:
    return cv2.dnn.readNet(
        model="data/model/age_deploy.prototxt",
        config="data/model/age_net.caffemodel"
    )


@st.cache_resource
def load_gender_model() -> cv2.dnn.Net:
    return cv2.dnn.readNet(
        model="data/model/gender_deploy.prototxt",
        config="data/model/gender_net.caffemodel"
    )


face_model: cv2.dnn.Net = load_face_model()
age_model: cv2.dnn.Net = load_age_model()
gender_model: cv2.dnn.Net = load_gender_model()


uploaded_file = st.file_uploader("Choose an image")
st.markdown("---")

if uploaded_file is not None:
    pil_image = Image.open(uploaded_file).convert('RGB')
    image: np.ndarray = np.array(pil_image)
    image: np.ndarray = cv2.cvtColor(
        src=image, code=cv2.COLOR_RGB2BGR
    )
    height, width = image.shape[:2]

    bounding_boxes: List[
        Tuple[int, int, int, int]
    ] = get_face_boxes(
        model=face_model, image=image
    )

    image_to_draw: np.ndarray = image.copy()
    padding: int = 20
    for x1, y1, x2, y2 in bounding_boxes:
        age, gender = get_age_gender(
            age_model=age_model, gender_model=gender_model,
            image=image[
                  max(0, y1-padding):min(y2+padding, height-1),
                  max(0, x1-padding):min(x2+padding, width-1)
            ]
        )
        cv2.rectangle(
            img=image_to_draw, pt1=(x1, y1), pt2=(x2, y2),
            color=(0, 255, 0), thickness=2,
            lineType=cv2.LINE_4
        )
        cv2.putText(
            img=image_to_draw, text=f"{gender} ({age})",
            org=(x1, y1 - 10), fontFace=cv2.FONT_HERSHEY_SIMPLEX,
            fontScale=0.5, color=(0, 0, 255), thickness=1,
            lineType=cv2.LINE_AA
        )

    image_to_draw = cv2.cvtColor(
        src=image_to_draw, code=cv2.COLOR_BGR2RGB
    )
    st.image(
        image=image_to_draw, caption="Predict result"
    )
