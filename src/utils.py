import numpy as np
import cv2
from typing import List, Tuple


def get_face_boxes(
        model: cv2.dnn.Net, image: np.ndarray,
        conf_threshold: float = 0.7
) -> List[
    Tuple[int, int, int, int]
]:
    """
    Get face boxes
    :param model: model
    :param image: image
    :param conf_threshold: conf threshold
    :return: facebox
    """
    image: np.ndarray = image.copy()
    height, width = image.shape[:2]
    blob: np.ndarray = cv2.dnn.blobFromImage(
        image=image, scalefactor=1.0, size=(300, 300),
        mean=[104, 117, 123], swapRB=True, crop=False
    )

    model.setInput(blob=blob)
    detections: np.ndarray = model.forward()

    bounding_boxes: List[
        Tuple[int, int, int, int]
    ] = []
    for i in range(detections.shape[2]):
        confidence = detections[0, 0, i, 2]
        if confidence > conf_threshold:
            x1 = int(detections[0, 0, i, 3] * width)
            y1 = int(detections[0, 0, i, 4] * height)
            x2 = int(detections[0, 0, i, 5] * width)
            y2 = int(detections[0, 0, i, 6] * height)
            bounding_boxes.append((x1, y1, x2, y2))

    return bounding_boxes


age_list: List[str] = [
    '0-2', '4-6', '8-12', '15-20', '25-32',
    '38-43', '48-53', '60-100'
]
gender_list: List[str] = ['Male', 'Female']


def get_age_gender(
        age_model: cv2.dnn.Net, gender_model: cv2.dnn.Net,
        image: np.ndarray
) -> Tuple[str, str]:
    """
    Get age, gender predict
    :param age_model: model age
    :param gender_model: model gender
    :param image: image
    :return: age, gender
    """
    blob: np.ndarray = cv2.dnn.blobFromImage(
        image=image, scalefactor=1.0, size=(227, 227),
        mean=[78.4263377603, 87.7689143744, 114.895847746], swapRB=False
    )

    age_model.setInput(blob=blob)
    age_predict: np.ndarray = age_model.forward()
    age: str = age_list[age_predict[0].argmax()]

    gender_model.setInput(blob=blob)
    gender_predict: np.ndarray = gender_model.forward()
    gender: str = gender_list[gender_predict[0].argmax()]

    return age, gender
